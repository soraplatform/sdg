---
title: "About"
icon: "/images/icons/heart.svg"
description: "We need our volunteers and partners to be cognizant of that goal as much as possible."
---



SCENAC was founded on December 11, 2003 a Non-Government Organization (NGO) representing the disadvantaged elderly and children sector as a partner  <!-- extension agency --> of the Department of Social Welfare and Development (DSWD).

We focus on humanitarian and social development assistance in the form of:
- medical treatment <!-- advocating urgently needed -->
- food assistance for maintenance and survival
<!--  as charity or service patients for serious health conditions and providing temporary shelter for troubled and displaced street children. -->

As a non-stock, non-profit organization <!-- , it has no budgeting allocations from both the local and national government. As such it --> we operate on the principles of:

1. Volunteerism 
2. Cooperation 
3. Multi-Stakeholdership



## Vision

To build an empowered society of financially able, physically fit, mentally strong and spiritually elevated human being led by happy and highly productive Senior Citizens working as the most valued partners in nation building and socio-economic development.

<!-- To be a partner in nation-building and socio-economic development by providing quality and innovative services that are responsive to the growing needs of our people.

We will strive for efficiency, excellence and integrity, conscientiously utilizing our resources to fulfill our commitments and contribute to improve the quality of life of every Filipino. -->


## Mission

1. To empower SCENAC Members in striving for efficiency, excellence and integrity in the society;

2. To implement programs for proper nutrition, health & wellness to improve the quality of life of every human being.

3. To provide a diversified and comprehensive range of quality and innovative products and services that are responsive to the growing needs of the Senior Sector, the depressed, marginalized, persons with disabilities, the poorest and the most vulnerable class of our society.

4. To provide free education and or financial assistance for those who cannot afford the rising cost of education in our country.


<!-- To extend our services by creating and providing sustainable initiatives, such as livelihood and job opportunities, at the community level for the urban poor and the out-of-school youth.  -->
<!-- , using SCENAC quality, environment friendly and affordable consumer’s products as primary base income and activity. -->


## Rationale

The issue of aging has become a major concern worldwide. <!-- not only of the developed countries but also the developing and underdeveloped countries. -->

Curative and preventive interventions that seek to support the special needs of the senior citizen and to reduce risks associated with old age shall be provided.

Senior Citizens Act No. 3994 was enacted into law to protect seniors from abuse, exploitation and discrimination. It aims to establish, within the famity and community level, the prevention of physical, emotional and financial abuse, neglect and abandonment of senior citizens. 

SCENAC supports this law through:
- training and awareness campaigns that raise public awareness on the problems confronting senior citizens
- provide means to <!--  and to equip the public with the necessary “know-how" to --> allow the public to <!-- prevent and --> detect and report the problems that afflict seniors<!--  problem areas concerning these sectors. -->

<!-- The full implementation of this act shall bring immediate delivery of necessary services for these sectors such as recreational, educational, health and social program designed for the full enjoyment and benefit of this very important sector of the country. -->


<!-- ## Where does the Code of Conduct apply?

If you join in or contribute to the Food Rescue Philippines ecosystem in any way, you are encouraged to follow the Code of Conduct while doing so.

Explicit enforcement of the Code of Conduct applies to all rescues run by Food Rescue Philippines, and should be applied indifferently to everyone, including all staff, contractors, volunteers, and students involved. It will also apply to any online communities, forums, and other mediums of communication operated by Food Rescue Philippines.
 -->

<!-- ## Unwelcome Behavior

These actions are explicitly forbidden in Food Rescue Philippines rescues:

- Grab and go (the Expressing or provoking:
  - insulting, demeaning, hateful, or threatening remarks;
  - discrimination based on age, nationality, race, (dis)ability, gender (identity or expression), sexuality, religion, or similar personal characteristic;
  - bullying or systematic harassment;
  - unwelcome sexual advances, including sexually explicit content;
- Being under the influence of alcohol, intoxicants while doing food rescue.
 -->





## History

Date | Notes
--- | ---
February 3, 2004 | SCENAC Phils, Inc., started as a local organization known as "Federation of Senior Citizens Association of the Philippines National Capital Region Paranaque Chapter, Inc (FSCAP-NCR-Paranaque City Chapter, Inc.) with SEC REG. NO. CN200401598. 
February 10, 2013 | The name was amended and approved as Senior Citizens Expanded New Association and Coalition (SCENAC) of the Philippines, Inc. under the same SEC Registration No. CN200401598.


<!-- in response to RA 7432 of 1992 and as amended by RA 9257 of 2003; And to keep the organization align to RA. NO. 9994, also known as the "EXPANDED SENIOR CITIZENS ACT OF 2010," AN ACT GRANTING ADDITIONAL BENEFITS AND PRIVILEGES TO SENIOR CITIZENS, FURTHER AMENDING REPUBLIC ACT NO. 7432 OF 1992 AS AMENDED BY REPUBLIC ACT NO. 9257 OF 2003,
It is a Non-Government Organization (NGO), non-profit, non-sectarian, non-partisan and humanitarian organization duly organized to represent the senior citizens and PWD sectors of our society. It is accredited with the Department of Social Welfare and Development (DSWD) and in consonant with constitutional principles that adopted the declared policies of the Expanded Senior Citizens Act of 2010 (RA 9994). -->

SEC REG. NO. CN200401598 

DSWD REG. NO. NCR-2004-R-283 

(Formerly: Federation of Senior Citizens Association of the Philippines National Capital Region, Paranaque City Chapter)

SCENAC
Unit B Block 5 Lot 1 J. Hernandez Street San Antonio Valley 7 Pulang Lupa Dos Las Pinas City Metro Manila Telephone Numbers: 0917-701-2361 & 0999-691-0011
