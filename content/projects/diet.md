---
title: "Diet-Disease App"
date: 2023-04-20
author: Juan
icon: /icons/main.png
"projects/tags": [ Apps ]
image: "https://www.pantrypoints.com/og/diet.jpg"
description: We have a Diet-Disease App for common diseases
---


We have a Diet-Disease App for common diseases at https://play.google.com/store/apps/details?id=com.pantrypoints.diet

![Diet-Disease App](https://www.pantrypoints.com/og/diet.jpg)



