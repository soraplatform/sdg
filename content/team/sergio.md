---
title: "Sergio Del Cano"
nickname: "Sergio"
# email: foodrescueph@gmail.com
image: "/avatars/sergio.jpg"
school: ''
# wallet: "/images/gcashmac.jpg"
# class: 23
jobtitle: "Consultant"
# linkedinurl: "https://www.linkedin.com/in/megancui"
location: "Caloocan City"
promoted: true
weight: 1
description: Sergio contributes to SCENAC through his physical work and by sharing his experience in healthcare
---


Sergio contributes to SCENAC through his physical work and by sharing his experience in healthcare