---
title: "Jose Mordeno"
nickname: "Jose"
# email: foodrescueph@gmail.com
image: "/avatars/jose.jpg"
school: 'University of Mindanao'
# wallet: "/images/gcashmac.jpg"
# class: 23
jobtitle: "Inventor"
# linkedinurl: "https://www.linkedin.com/in/megancui"
location: "Las Pinas City"
promoted: true
weight: 1
description: Jose has been active in social service since the 1970's in Davao and in Metro Manila
---


Jose is a self-made inventor and homeopathy practitioner who has a big heart for social service. He regularly participates in medical missions all over the Philippines.
