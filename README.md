## SCENAC

1.5 years 

Senior Citizens Expanded New Association and Coalition of the Philippines, Inc


<br />Class of 20{{ .Params.class }}

Pages
sdg-14n.pages.dev

SCENAC stands for Senior Citizens Expanded New Association and Coalition of the Philippines, an NGO established for Senior Citizens. 

We hold events and activities for seniors such as health programs and livelihood projects using our points-based system. 

SCENAC stands for Senior Citizens Expanded New Association and Coalition of the Philippines, Inc. It is a non-government organization located in the Las Pinas City serving the needs of the senior citizens in the Philippines. 

[
  {
    "name": "",
    "image": "/images/team/jun.jpg",
    "school": "De La Salle University",
    "class": 2001,
    "jobtitle": "Web Development"
  }
]


{{< expand "What are the benefits that Senior Citizens get?" >}}
Republic Act 9994 is the Expanded Senior Citizen's Act of 2010. It gives the following benefits:
- Free medical and dental diagnostic and lab fees in government hospitals
- 20% discount in:
  - medicines
  - hotels, restaurants, funeral parlors, and recreation centers
  - theaters, cinema houses, concert halls
  - medical and dental services and diagnostic and lab fees in private hospitals
  - in domestic transportation
{{< /expand >}}


  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id={{ .Site.Params.googleAnalitycsID }}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{ .Site.Params.googleAnalitycsID }}');
  </script>
